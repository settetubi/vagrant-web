#!/usr/bin/env bash

# updating repository
sudo apt-get install software-properties-common
sudo apt-add-repository ppa:ansible/ansible
sudo apt-get -y update
sudo apt-get -y dist-upgrade

sudo apt-get -y install ansible

sudo ansible-playbook /vagrant/ansible/playbook.yml -i /vagrant/ansible/hosts --connection=local
