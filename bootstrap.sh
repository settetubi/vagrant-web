#!/bin/bash

# updating repository
sudo apt-get install software-properties-common
sudo apt-add-repository ppa:ansible/ansible
sudo apt-get -y update
sudo apt-get -y dist-upgrade

sudo apt-get install ansible

# installing apache
sudo apt-get -y install apache2

# installing Mysql  and dipendencies. Also set up mysql root password with prompt
sudo debconf-set-selections <<< 'mysql-server-5.7 mysql-server/root_password password rootpass'
sudo debconf-set-selections <<< 'mysql-server-5.7 mysql-server/root_password_again password rootpass'
sudo apt-get -y install mysql-server php-mbstring php7.0-mbstring php-gettext

# installing PHP and it's dependencies
sudo apt-get -y install php7.0 libapache2-mod-php7.0 php7.0-mcrypt php7.0-mysql

